import 'dart:math';

import 'package:fl_chart/fl_chart.dart';
import 'package:fl_charts_practice_flutter/utilities/constants.dart';
import 'package:flutter/material.dart';

class DetailsPage extends StatefulWidget {
  final String title;

  const DetailsPage({Key key, this.title}) : super(key: key);

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  int touchedGroupIndex;
  List<BarChartGroupData> rawBarGroups;
  List<BarChartGroupData> averageBarGroups;
  List<BarChartGroupData> showingBarGroups;

  @override
  void initState() {
//    final items = [
//      makeGroupData(0),
//      makeGroupData(1),
//      makeGroupData(2),
//      makeGroupData(3),
//      makeGroupData(4),
//      makeGroupData(5),
//      makeGroupData(6),
//      makeGroupData(7),
//      makeGroupData(8),
//      makeGroupData(9),
//      makeGroupData(10),
//      makeGroupData(11),
//      makeGroupData(12),
//      makeGroupData(13),
//    ];
//
//    rawBarGroups = items;
//
//    showingBarGroups = rawBarGroups;

    List rawData = makeData();
    List<BarChartGroupData> items = [];
    int index = 0;
    for (int i = 0; i < rawData.length; i = i + 60) {
      items.add(makeGroupData(rawData.getRange(i, i + 60).toList(), index));
      index++;
    }
    rawBarGroups = items;
    averageBarGroups = getHourlyAverage(rawBarGroups);
//    showingBarGroups = rawBarGroups;
    showingBarGroups = averageBarGroups;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kDarkBG,
      appBar: AppBar(
        centerTitle: false,
        title: Text(
          widget.title,
          textAlign: TextAlign.left,
        ),
        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.calendar_today),
            onPressed: () {},
          ),
        ],
        flexibleSpace: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(20),
              bottomLeft: Radius.circular(20),
            ),
            gradient: LinearGradient(
              colors: kAppBarDark,
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
        ),
      ),
      body: Container(
        child: SingleChildScrollView(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              BarChart(
                BarChartData(
                  titlesData: FlTitlesData(
                    show: true,
                    leftTitles: SideTitles(showTitles: false),
                    topTitles: SideTitles(showTitles: false),
                    rightTitles: SideTitles(
                      showTitles: true,
                      textStyle: TextStyle(
                        color: Color(0xff80ffffff),
                        fontSize: 10,
                      ),
                      margin: 32,
                      getTitles: (value) {
                        switch (value.toInt()) {
                          case 0:
                            return '0';
                          case 100:
                            return '100';
                          case 200:
                            return '200';
                          case 300:
                            return '300';
                          case 400:
                            return '400';
                          case 500:
                            return '500';
                          case 600:
                            return '600';
                          case 700:
                            return '700';
                          case 800:
                            return '800';
                          default:
                            return '';
                        }
                      },
                    ),
                    bottomTitles: SideTitles(
                      interval: 1,
                      showTitles: true,
                      textStyle: TextStyle(
                        color: Color(0xff80ffffff),
                        fontSize: 10,
                      ),
                      margin: 16,
                      getTitles: (double value) {
                        switch (value.toInt()) {
                          case 0:
                            return '6:00';
                          case 1:
                            return '7:00';
                          case 2:
                            return '8:00';
                          case 3:
                            return '9:00';
                          case 4:
                            return '10:00';
                          case 5:
                            return '11:00';
                          case 6:
                            return '12:00';
                          case 7:
                            return '13:00';
                          case 8:
                            return '14:00';
                          case 9:
                            return '15:00';
                          case 10:
                            return '16:00';
                          case 11:
                            return '17:00';
                          case 12:
                            return '18:00';
                          case 13:
                            return '19:00';
                          default:
                            return '';
                        }
                      },
                    ),
                  ),
                  borderData: FlBorderData(show: false),
                  gridData: FlGridData(show: false),
                  minY: 0,
                  maxY: 800,
                  barTouchData: BarTouchData(
//                    handleBuiltInTouches: false,
//                    allowTouchBarBackDraw: true,
                      enabled: true,
//                    touchTooltipData: BarTouchTooltipData(
//                      tooltipBgColor: Colors.grey,
//                      getTooltipItem: (_a, _b, _c, _d) => null,
//                    ),
                      touchCallback: (response) {
                        if (response.spot == null) {
                          setState(() {
                            touchedGroupIndex = -1;
                            showingBarGroups = List.of(averageBarGroups);
                          });
                          return;
                        }

                        touchedGroupIndex = response.spot.touchedBarGroupIndex;

                        setState(
                          () {
//                        if (response.touchInput is FlLongPressEnd || response.touchInput is FlPanEnd) {
//                          touchedGroupIndex = -1;
//                          showingBarGroups = List.of(rawBarGroups);
//                        } else {
//                          showingBarGroups = List.of(rawBarGroups);
//                          if (touchedGroupIndex != -1) {
//                            showingBarGroups[touchedGroupIndex] = showingBarGroups[touchedGroupIndex].copyWith(
//                              barRods: showingBarGroups[touchedGroupIndex].barRods.map((rod) {
//                                return rod.copyWith(color: Colors.blue);
//                              }).toList(),
//                            );
//                          }
//                        }

                            if (response.touchInput is FlLongPressEnd || response.touchInput is FlPanEnd) {
                              showingBarGroups = List.of(averageBarGroups);
                              if (touchedGroupIndex != -1) {
                                showingBarGroups = showingBarGroups
                                    .map((barGroup) => barGroup.copyWith(
                                        barRods: barGroup.barRods.map((rod) => rod.copyWith(width: 0)).toList()))
                                    .toList();

                                showingBarGroups[touchedGroupIndex] = showingBarGroups[touchedGroupIndex].copyWith(
                                    barsSpace: 3,
                                    barRods: rawBarGroups[touchedGroupIndex]
                                        .barRods
                                        .map((rod) => rod.copyWith(width: 2))
                                        .toList());
                              }
                            }
                          },
                        );

                        ///
                      }),
                  barGroups: showingBarGroups,
                ),
                swapAnimationDuration: const Duration(milliseconds: 500),
              ),
              LineChart(
                tileBilanzData(),
                swapAnimationDuration: const Duration(milliseconds: 1000),
              ),
              LineChart(
                tileVerbrauchData(),
                swapAnimationDuration: const Duration(milliseconds: 1000),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

BarChartGroupData makeGroupData(List data, int xIndex) {
  List<BarChartRodData> barRodData = [];
  for (var data in data) {
    barRodData.add(
      BarChartRodData(
        y: data,
        color: Color(0xfffad961),
        width: 3,
      ),
    );
  }

  return BarChartGroupData(
    x: xIndex,
    barsSpace: 2,
    barRods: barRodData,
  );
}

List<double> makeData() => [for (int i = 0; i < 840; i++) Random().nextInt(800).toDouble()];

List<BarChartGroupData> getHourlyAverage(List<BarChartGroupData> rawData) {
  List<BarChartGroupData> hourlyAverage = [];

  for (BarChartGroupData groupData in rawData) {
    double sum = 0;
    for (BarChartRodData rod in groupData.barRods) {
      sum += rod.y;
    }
    hourlyAverage.add(BarChartGroupData(
      x: groupData.x,
      barsSpace: 2,
      barRods: [
        BarChartRodData(
          y: sum / groupData.barRods.length,
          color: Color(0xfffad961),
          width: 10,
        )
      ],
    ));
  }

  return hourlyAverage;
}

LineChartData tileBilanzData() {
  const double period = 2 * pi;
  List<double> x = [for (double i = 0; i <= period; i += .01) i];
  List<FlSpot> dataSin = [for (int i = 0; i < x.length; i++) FlSpot(0, 0)];
  List<FlSpot> dataCos = [for (int i = 0; i < x.length; i++) FlSpot(0, 0)];
  List<FlSpot> dataTan = [for (int i = 0; i < x.length; i++) FlSpot(0, 0)];

  dataSin.clear();
  double aSin = Random().nextDouble() * 2;
  double pSin = Random().nextDouble() * 2 * pi;
  for (double x in x) {
    dataSin.add(FlSpot(x, aSin * sin(x + pSin)));
  }

  dataCos.clear();
  double aCos = Random().nextDouble() * 2;
  double pCos = Random().nextDouble() * 2 * pi;
  for (double x in x) {
    dataCos.add(FlSpot(x, aCos * cos(.5 * x + pCos)));
  }

  dataTan.clear();
  double aTan = Random().nextDouble() * 2;
  double pTan = Random().nextDouble() * 2 * pi;
  for (double x in x) {
    dataTan.add(FlSpot(x, aTan * atan(x - pTan)));
  }

  List<Color> gradientColorsSin = [
    const Color(0xfffab62b),
    const Color(0xff232d37),
  ];

  List<Color> gradientColorsCos = [
    const Color(0xff8ed14f),
    const Color(0xff232d37),
  ];

  List<Color> gradientColorsTan = [
    const Color(0xff1e53db),
    const Color(0xff232d37),
  ];

  return LineChartData(
    clipData: FlClipData.all(),
    lineTouchData: LineTouchData(enabled: true),
    titlesData: FlTitlesData(
      show: true,
      leftTitles: SideTitles(showTitles: false),
      topTitles: SideTitles(showTitles: false),
      rightTitles: SideTitles(
        showTitles: true,
        textStyle: TextStyle(
          color: Color(0xff80ffffff),
          fontSize: 10,
        ),
        margin: 20,
        getTitles: (value) {
          if (value == -2) {
            return '0';
          } else if (value == -1.5) {
            return '100';
          } else if (value == -1) {
            return '200';
          } else if (value == -0.5) {
            return '300';
          } else if (value == 0) {
            return '400';
          } else if (value == 0.5) {
            return '500';
          } else if (value == 1) {
            return '600';
          } else if (value == 1.5) {
            return '700';
          } else if (value == 2) {
            return '800';
          } else {
            return '';
          }
        },
      ),
      bottomTitles: SideTitles(
        interval: 1,
        showTitles: true,
        textStyle: TextStyle(
          color: Color(0xff80ffffff),
          fontSize: 10,
        ),
        margin: 16,
        getTitles: (double value) {
          switch (value.toInt()) {
            case 0:
              return '6:00';
            case 1:
              return '7:00';
            case 2:
              return '8:00';
            case 3:
              return '9:00';
            case 4:
              return '10:00';
            case 5:
              return '11:00';
            case 6:
              return '12:00';
            case 7:
              return '13:00';
            case 8:
              return '14:00';
            case 9:
              return '15:00';
            case 10:
              return '16:00';
            case 11:
              return '17:00';
            case 12:
              return '18:00';
            case 13:
              return '19:00';
            default:
              return '';
          }
        },
      ),
    ),
    borderData: FlBorderData(show: false),
    gridData: FlGridData(show: false),
    minX: 0,
    maxX: period,
    minY: -2,
    maxY: 2,
    lineBarsData: [
      LineChartBarData(
        spots: dataSin,
        isCurved: true,
        gradientFrom: Offset(0, 0),
        gradientTo: Offset(0, 1),
        colors: gradientColorsSin,
        barWidth: 2,
        isStrokeCapRound: true,
        dotData: FlDotData(
          show: false,
        ),
        belowBarData: BarAreaData(
          show: true,
          gradientFrom: Offset(0, 0),
          gradientTo: Offset(0, .9),
          colors: gradientColorsSin.map((color) => color.withOpacity(0.1)).toList(),
        ),
      ),
      LineChartBarData(
        spots: dataCos,
        isCurved: true,
        gradientFrom: Offset(0, 0),
        gradientTo: Offset(1, 1),
        colors: gradientColorsCos,
        barWidth: 2,
        dotData: FlDotData(
          show: false,
        ),
        belowBarData: BarAreaData(
          show: true,
          gradientFrom: Offset(0, 0),
          gradientTo: Offset(0, .9),
          colors: gradientColorsCos.map((color) => color.withOpacity(0.1)).toList(),
        ),
      ),
      LineChartBarData(
        spots: dataTan,
        isCurved: true,
        gradientFrom: Offset(0, 0),
        gradientTo: Offset(1, 1),
        colors: gradientColorsTan,
        barWidth: 2,
        dotData: FlDotData(
          show: false,
        ),
        belowBarData: BarAreaData(
          show: true,
          gradientFrom: Offset(0, 0),
          gradientTo: Offset(0, .9),
          colors: gradientColorsTan.map((color) => color.withOpacity(0.1)).toList(),
        ),
      ),
    ],
  );
}

LineChartData tileVerbrauchData() {
  List<FlSpot> data1 = [
    FlSpot(0, 0),
    ...[for (int i = 1; i < 4; i++) FlSpot(i.toDouble(), Random().nextInt(800).toDouble())],
    FlSpot(4, 0)
  ];
  List<FlSpot> data2 = [
    FlSpot(0, 0),
    ...[for (int i = 1; i < 4; i++) FlSpot(i.toDouble(), Random().nextInt(800).toDouble())],
    FlSpot(4, 0)
  ];
  List<FlSpot> data3 = [
    FlSpot(0, 0),
    ...[for (int i = 1; i < 4; i++) FlSpot(i.toDouble(), Random().nextInt(800).toDouble())],
    FlSpot(4, 0)
  ];
  List<FlSpot> data4 = [
    FlSpot(0, 0),
    ...[for (int i = 1; i < 4; i++) FlSpot(i.toDouble(), Random().nextInt(800).toDouble())],
    FlSpot(4, 0)
  ];

  const Color kGreen = Color(0xff8ee65a);
  const Color kRed = Color(0xffe84f4f);
  const Color kBlue = Color(0xff1653f8);
  const Color kYellow = Color(0xfffbb700);

  const double width = 1;

  return LineChartData(
    clipData: FlClipData.all(),
    gridData: FlGridData(show: false),
    lineTouchData: LineTouchData(enabled: true),
    titlesData: FlTitlesData(
      show: true,
      leftTitles: SideTitles(showTitles: false),
      topTitles: SideTitles(showTitles: false),
      rightTitles: SideTitles(
        showTitles: true,
        textStyle: TextStyle(
          color: Color(0xff80ffffff),
          fontSize: 10,
        ),
        margin: 20,
        getTitles: (value) {
          switch (value.toInt()) {
            case 0:
              return '0';
            case 100:
              return '100';
            case 200:
              return '200';
            case 300:
              return '300';
            case 400:
              return '400';
            case 500:
              return '500';
            case 600:
              return '600';
            case 700:
              return '700';
            case 800:
              return '800';
            default:
              return '';
          }
        },
      ),
      bottomTitles: SideTitles(
        interval: 1,
        showTitles: true,
        textStyle: TextStyle(
          color: Color(0xff80ffffff),
          fontSize: 10,
        ),
        margin: 16,
        getTitles: (double value) {
          switch (value.toInt()) {
            case 0:
              return '6:00';
            case 1:
              return '7:00';
            case 2:
              return '8:00';
            case 3:
              return '9:00';
            case 4:
              return '10:00';
            case 5:
              return '11:00';
            case 6:
              return '12:00';
            case 7:
              return '13:00';
            case 8:
              return '14:00';
            case 9:
              return '15:00';
            case 10:
              return '16:00';
            case 11:
              return '17:00';
            case 12:
              return '18:00';
            case 13:
              return '19:00';
            default:
              return '';
          }
        },
      ),
    ),
    borderData: FlBorderData(show: false),
    minX: 0,
    maxX: 4,
    minY: 0,
    maxY: 800,
    lineBarsData: [
      LineChartBarData(
        spots: data1,
        isCurved: false,
        dotData: FlDotData(show: false),
        barWidth: width,
        colors: [kBlue],
      ),
      LineChartBarData(
        spots: data2,
        isCurved: false,
        dotData: FlDotData(show: false),
        barWidth: width,
        colors: [kYellow],
      ),
      LineChartBarData(
        spots: data3,
        isCurved: false,
        dotData: FlDotData(show: false),
        barWidth: width,
        colors: [kRed],
      ),
      LineChartBarData(
        spots: data4,
        isCurved: false,
        dotData: FlDotData(show: false),
        barWidth: width,
        colors: [kGreen],
      ),
    ],
  );
}
