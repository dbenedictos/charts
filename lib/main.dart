import 'package:charts_flutter/flutter.dart' as ChartsFlutter;
import 'package:fl_chart/fl_chart.dart' as FLCharts;
import 'package:fl_charts_practice_flutter/utilities/constants.dart';
import 'package:fl_charts_practice_flutter/widgets/graph_tile.dart';
import 'package:flutter/material.dart';

import 'model/chart_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kDarkBG,
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.autorenew),
          onPressed: () {
            setState(() {});
          },
        ),
        body: SafeArea(
          child: GridView.count(
            childAspectRatio: 187 / 224,
            crossAxisCount: 2,
            crossAxisSpacing: 8,
            mainAxisSpacing: 8,
            padding: const EdgeInsets.all(16),
            children: [
              GraphTile(
                title: 'Wechselrichter',
                subtitle: '2.02 kWh',
                child: FLCharts.BarChart(
                  FLChartModel.tileWechselrichterData(),
                  swapAnimationDuration: const Duration(seconds: 1),
                ),
              ),
              GraphTile(
                title: '',
                subtitle: '',
                child: TimeSeriesBar(
                  TimeSeriesBar._createSampleData(),
                  animate: true,
                ),
              ),
              GraphTile(
                title: 'Bilanz',
                subtitle: '2.02 kWh',
                child: FLCharts.LineChart(
                  FLChartModel.tileBilanzData(),
                  swapAnimationDuration: const Duration(seconds: 1),
                ),
              ),
              GraphTile(
                title: '',
                subtitle: '',
                child: Container(),
              ),
              GraphTile(
                title: 'Verbrauch',
                subtitle: '22.07.2020',
                child: FLCharts.LineChart(
                  FLChartModel.tileVerbrauchData(),
                  swapAnimationDuration: const Duration(seconds: 1),
                ),
              ),
              GraphTile(
                title: '',
                subtitle: '',
                child: Container(),
              ),
            ],
          ),
        ));
  }
}

class TimeSeriesBar extends StatelessWidget {
  final List<ChartsFlutter.Series<TimeSeriesSales, DateTime>> seriesList;
  final bool animate;

  TimeSeriesBar(this.seriesList, {this.animate});

  /// Creates a [TimeSeriesChart] with sample data and no transition.
  factory TimeSeriesBar.withSampleData() {
    return new TimeSeriesBar(
      _createSampleData(),
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new ChartsFlutter.TimeSeriesChart(
      seriesList,
      animate: animate,
      // Set the default renderer to a bar renderer.
      // This can also be one of the custom renderers of the time series chart.
      defaultRenderer: new ChartsFlutter.BarRendererConfig<DateTime>(),
      // It is recommended that default interactions be turned off if using bar
      // renderer, because the line point highlighter is the default for time
      // series chart.
      defaultInteractions: false,
      // If default interactions were removed, optionally add select nearest
      // and the domain highlighter that are typical for bar charts.
      behaviors: [new ChartsFlutter.SelectNearest(), new ChartsFlutter.DomainHighlighter()],
    );
  }

  /// Create one series with sample hard coded data.
  static List<ChartsFlutter.Series<TimeSeriesSales, DateTime>> _createSampleData() {
    final data = [
      new TimeSeriesSales(new DateTime(2017, 9, 1), 5),
      new TimeSeriesSales(new DateTime(2017, 9, 2), 5),
      new TimeSeriesSales(new DateTime(2017, 9, 3), 25),
      new TimeSeriesSales(new DateTime(2017, 9, 4), 100),
      new TimeSeriesSales(new DateTime(2017, 9, 5), 75),
      new TimeSeriesSales(new DateTime(2017, 9, 6), 88),
      new TimeSeriesSales(new DateTime(2017, 9, 7), 65),
      new TimeSeriesSales(new DateTime(2017, 9, 8), 91),
      new TimeSeriesSales(new DateTime(2017, 9, 9), 100),
      new TimeSeriesSales(new DateTime(2017, 9, 10), 111),
      new TimeSeriesSales(new DateTime(2017, 9, 11), 90),
      new TimeSeriesSales(new DateTime(2017, 9, 12), 50),
      new TimeSeriesSales(new DateTime(2017, 9, 13), 40),
      new TimeSeriesSales(new DateTime(2017, 9, 14), 30),
      new TimeSeriesSales(new DateTime(2017, 9, 15), 40),
      new TimeSeriesSales(new DateTime(2017, 9, 16), 50),
      new TimeSeriesSales(new DateTime(2017, 9, 17), 30),
      new TimeSeriesSales(new DateTime(2017, 9, 18), 35),
      new TimeSeriesSales(new DateTime(2017, 9, 19), 40),
      new TimeSeriesSales(new DateTime(2017, 9, 20), 32),
      new TimeSeriesSales(new DateTime(2017, 9, 21), 31),
    ];

    return [
      new ChartsFlutter.Series<TimeSeriesSales, DateTime>(
        id: 'Sales',
        colorFn: (_, __) => ChartsFlutter.Color.white,
        domainFn: (TimeSeriesSales sales, _) => sales.time,
        measureFn: (TimeSeriesSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }
}

/// Sample time series data type.
class TimeSeriesSales {
  final DateTime time;
  final int sales;

  TimeSeriesSales(this.time, this.sales);
}
