import 'dart:math';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class FLChartModel {
  static BarChartData tileWechselrichterData() {
    List<BarChartRodData> dataBar = [];

    List<Color> gradientColorsBar = [
      const Color(0xfffad961),
      const Color(0xfff76b1c),
    ];

    for (int i = 0; i < 35; i++) {
      if (i < 18) {
        dataBar.add(
          BarChartRodData(
            y: Random().nextInt(9) + 10.toDouble(),
            color: Color(0xfffad961),
            width: 2,
          ),
        );
      } else {
        dataBar.add(BarChartRodData(
          y: 0,
          color: gradientColorsBar.first,
          width: 2,
          backDrawRodData: BackgroundBarChartRodData(
            show: true,
            y: 2,
            color: Colors.transparent, //Color(0xff3a3748),
          ),
        ));
      }
    }

    return BarChartData(
      titlesData: FlTitlesData(show: false),
      borderData: FlBorderData(show: false),
      gridData: FlGridData(show: false),
      minY: 0,
      maxY: 20,
      barTouchData: BarTouchData(enabled: false),
      barGroups: [
        BarChartGroupData(
          x: 40,
          barRods: dataBar,
        ),
      ],
    );
  }

  static LineChartData tileBilanzData() {
    const double period = 2 * pi;
    List<double> x = [for (double i = 0; i <= period; i += .01) i];
    List<FlSpot> dataSin = [for (int i = 0; i < x.length; i++) FlSpot(0, 0)];
    List<FlSpot> dataCos = [for (int i = 0; i < x.length; i++) FlSpot(0, 0)];
    List<FlSpot> dataTan = [for (int i = 0; i < x.length; i++) FlSpot(0, 0)];

    dataSin.clear();
    double aSin = Random().nextDouble() * 2;
    double pSin = Random().nextDouble() * 2 * pi;
    for (double x in x) {
      dataSin.add(FlSpot(x, aSin * sin(x + pSin)));
    }

    dataCos.clear();
    double aCos = Random().nextDouble() * 2;
    double pCos = Random().nextDouble() * 2 * pi;
    for (double x in x) {
      dataCos.add(FlSpot(x, aCos * cos(.5 * x + pCos)));
    }

    dataTan.clear();
    double aTan = Random().nextDouble() * 2;
    double pTan = Random().nextDouble() * 2 * pi;
    for (double x in x) {
      dataTan.add(FlSpot(x, aTan * atan(x - pTan)));
    }

    List<Color> gradientColorsSin = [
      const Color(0xfffab62b),
      const Color(0xff232d37),
    ];

    List<Color> gradientColorsCos = [
      const Color(0xff8ed14f),
      const Color(0xff232d37),
    ];

    List<Color> gradientColorsTan = [
      const Color(0xff1e53db),
      const Color(0xff232d37),
    ];

    return LineChartData(
      clipData: FlClipData.all(),
      lineTouchData: LineTouchData(enabled: false),
      titlesData: FlTitlesData(
        show: false,
      ),
      borderData: FlBorderData(show: false),
      gridData: FlGridData(show: false),
      minX: 0,
      maxX: period,
      minY: -2,
      maxY: 2,
      lineBarsData: [
        LineChartBarData(
          spots: dataSin,
          isCurved: true,
          gradientFrom: Offset(0, 0),
          gradientTo: Offset(0, 1),
          colors: gradientColorsSin,
          barWidth: 2,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            gradientFrom: Offset(0, 0),
            gradientTo: Offset(0, .9),
            colors: gradientColorsSin.map((color) => color.withOpacity(0.1)).toList(),
          ),
        ),
        LineChartBarData(
          spots: dataCos,
          isCurved: true,
          gradientFrom: Offset(0, 0),
          gradientTo: Offset(1, 1),
          colors: gradientColorsCos,
          barWidth: 2,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            gradientFrom: Offset(0, 0),
            gradientTo: Offset(0, .9),
            colors: gradientColorsCos.map((color) => color.withOpacity(0.1)).toList(),
          ),
        ),
        LineChartBarData(
          spots: dataTan,
          isCurved: true,
          gradientFrom: Offset(0, 0),
          gradientTo: Offset(1, 1),
          colors: gradientColorsTan,
          barWidth: 2,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            gradientFrom: Offset(0, 0),
            gradientTo: Offset(0, .9),
            colors: gradientColorsTan.map((color) => color.withOpacity(0.1)).toList(),
          ),
        ),
      ],
    );
  }

  static LineChartData tileVerbrauchData() {
    List<FlSpot> data1 = [
      FlSpot(0, 0),
      ...[for (int i = 1; i < 4; i++) FlSpot(i.toDouble(), Random().nextDouble())],
      FlSpot(4, 0)
    ];
    List<FlSpot> data2 = [
      FlSpot(0, 0),
      ...[for (int i = 1; i < 4; i++) FlSpot(i.toDouble(), Random().nextDouble())],
      FlSpot(4, 0)
    ];
    List<FlSpot> data3 = [
      FlSpot(0, 0),
      ...[for (int i = 1; i < 4; i++) FlSpot(i.toDouble(), Random().nextDouble())],
      FlSpot(4, 0)
    ];
    List<FlSpot> data4 = [
      FlSpot(0, 0),
      ...[for (int i = 1; i < 4; i++) FlSpot(i.toDouble(), Random().nextDouble())],
      FlSpot(4, 0)
    ];

    const Color kGreen = Color(0xff8ee65a);
    const Color kRed = Color(0xffe84f4f);
    const Color kBlue = Color(0xff1653f8);
    const Color kYellow = Color(0xfffbb700);

    const double width = 1;

    return LineChartData(
      clipData: FlClipData.all(),
      gridData: FlGridData(show: false),
      lineTouchData: LineTouchData(enabled: false),
      titlesData: FlTitlesData(show: false),
      borderData: FlBorderData(show: false),
      minX: 0,
      maxX: 4,
      minY: 0,
      maxY: 1.2,
      lineBarsData: [
        LineChartBarData(
          spots: data1,
          isCurved: false,
          dotData: FlDotData(show: false),
          barWidth: width,
          colors: [kBlue],
        ),
        LineChartBarData(
          spots: data2,
          isCurved: false,
          dotData: FlDotData(show: false),
          barWidth: width,
          colors: [kYellow],
        ),
        LineChartBarData(
          spots: data3,
          isCurved: false,
          dotData: FlDotData(show: false),
          barWidth: width,
          colors: [kRed],
        ),
        LineChartBarData(
          spots: data4,
          isCurved: false,
          dotData: FlDotData(show: false),
          barWidth: width,
          colors: [kGreen],
        ),
      ],
    );
  }
}
