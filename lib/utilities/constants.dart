import 'package:flutter/material.dart';

const Color kLightBG = Color(0xfff5f5fc);
const Color kDarkBG = Color(0xff16151d);

const List<Color> kAppBarDark = [
  Color(0xff2a2834),
  Color(0xff1d1c24),
];

const Color kTabIndicatorDark = Color(0xff16151d);
